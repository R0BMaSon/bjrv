#!/bin/bash

# cd's into the upper level directory
cd ..

# cd's into the docker-compiling-of-petclinic directory/petclinic-docker
cd docker-compiling-of-petclinic/petclinic-docker

# build the dcoker image
docker build -t bjrv-a4-test .

# run the docker image
docker run -it --name bjrv-pet

# Setting a variable counter to count the seconds it takes to get a reponce from our system
counter=0

# Testing the instance for a responce, if it has not had a responce in 60 seconds it fires of an api message to the slack channel bjrv telling us the curl has failed!
until curl -s 'http://localhost:3080' | grep 'PetClinic'
do
	if (( counter > 12 ))
    then
    	curl -k -X POST \
--data '{"channel": "bjrv", "text": "so its only gone and failed numb-nuts ${BUILD_TAG} ${BUILD_URL} YOU ARE FAKE NEWS", "icon_url": "https://i.guim.co.uk/img/media/ae3eda66f16664b007451da66b591393b4f2e72b/0_46_3500_2100/master/3500.jpg?width=300&quality=85&auto=format&fit=max&s=c8b3cbb268d9b9aa2b2ddb7968f2b7f4", "username": "The Donald"}', https://hooks.slack.com/services/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
		exit 1

    fi
	sleep 5
    ((counter=counter+1))
done
