#!/bin/bash

#cd's into the upper level directory
cd ../environment

terraform init
#proforms an auto approved terraform apply of out terraform script!
terraform apply -auto-approve

terraform destroy -auto-approve
