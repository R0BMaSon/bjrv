#!/bin/bash

# cd's into the upper level directory
cd ..

# cd's into the docker-compiling-of-petclinic directory/petclinic-docker
cd docker-compiling-of-petclinic/petclinic-docker

# logs into to aws ecr
$(aws ecr get-login --no-include-email --region eu-west-2)

# proforms docker build on petclinic docker image
docker build -t bjrv-a4 .

# tags docker image with tag latest
docker tag bjrv-a4:latest 109964479621.dkr.ecr.eu-west-2.amazonaws.com/bjrv-a4:latest

# pushes docker image to the ecr repo
docker push 109964479621.dkr.ecr.eu-west-2.amazonaws.com/bjrv-a4:latest
