#!/bin/bash

# Setting a variable counter to count the seconds it takes to get a reponce from our system
counter=0

# Using the aws cli to control how many instances are up - in this case we want an minimum of 3 instances to be up
aws ecs update-service \
--desired-count "3"

# Testing the instance for a responce, if it has not had a responce in 2 and a half mins it fires of an api message to the slack channel bjrv telling us the curl has failed!

until curl -s 'http://a4-petclinic.grads.al-labs.co.uk' | grep 'PetClinic'
do
	if (( counter > 30 ))
    then
    	curl -k -X POST \
--data '{"channel": "bjrv", "text": "HO HO HO the jobs broken, fix it or youll be on the naughty list", "icon_url": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTLvLPTKkXjroXLSiDrNdde64uhWzv_n0TJW6UdK76WId5OcuVFfA", "username": "Santa"}', https://hooks.slack.com/services/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
		exit 1

    fi
	sleep 5
    ((counter=counter+1))
done
