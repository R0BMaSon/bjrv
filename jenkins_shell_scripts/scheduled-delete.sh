#!/bin/bash

# Using the aws cli to control how many instances are up - in this case we want zero instances to be up 
aws ecs update-service \
--desired-count "0"
