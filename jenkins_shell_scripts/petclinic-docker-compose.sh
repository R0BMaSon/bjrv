#!/bin/bash

# cd's into the upper level directory
cd ..

#delete docker images
docker rm -f $(docker ps -a | awk {'print $1'})
docker rmi $(docker images | awk {'print $3'})

# copying down the the jar file from the bjrv s3 bucket to the files directory of the petclinic-docker directory!
aws s3 cp s3://bjrv-bucket/spring-petclinic-2.0.0.jar docker-compiling-of-petclinic/petclinic-docker/files/spring-petclinic-2.0.0.jar

# cd's into the docker-compiling-of-petclinic directory
cd docker-compiling-of-petclinic

# proforms a docker-compose of the petclinic app and mysql docker images
docker-compose up -d

# Setting a variable counter to count the seconds it takes to get a reponce from our system
counter=0

# Testing the instance for a responce, if it has not had a responce in 60 seconds it fires of an api message to the slack channel bjrv telling us the curl has failed!
until curl -s 'http://localhost:3080' | grep 'PetClinic'
do
	if (( counter > 12 ))
    then
    	curl -k -X POST \
--data '{"channel": "bjrv", "text": "yo your job failed fix it!", "icon_url": "https://itisatechiesworld.files.wordpress.com/2015/01/cool-jenkins2x3.png", "username": "Ya boy JDog"}', https://hooks.slack.com/services/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
		exit 1

    fi
	sleep 5
    ((counter=counter+1))
done
