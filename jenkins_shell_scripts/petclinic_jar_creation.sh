#!/bin/bash

# Setting variables for the server name the database user and database passwords.
DBSRV=a4-dev-database.cibbo9q1lrf3.eu-west-2.rds.amazonaws.com
DBUSER=admin
DBPASS=password123

# git clones steves repo for petclinic
git clone https://bitbucket.org/JangleFett/petclinic.git

# cd into petclinic
cd petclinic

# First create the properties file
./mkprops ${DBSRV} petclinic ${DBUSER} ${DBPASS}

# Debug, check that config file has changed
cat src/main/resources/application.properties

source /etc/profile
/opt/apache-maven-3.6.0/bin/mvn -Dmaven.test.skip=true package

# Copy jar file to s3 bucket
aws s3 cp target/spring-petclinic-2.0.0.jar  s3://bjrv-bucket/spring-petclinic-2.0.0.jar
