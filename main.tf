module "jenkins" {
  source               = "./modules/jenkins"
  environment          = "${var.environment}"
  image_id             = "${var.image_id}"
  instance_type        = "${var.instance_type}"
  key_name             = "${var.key_name}"
  vpc_id               = "${var.vpc_id}"
  public_subnets_id    = "${var.public_subnets_id}"
  public_zone_id       = "${var.public_zone_id}"
}

module "vault" {
  source               = "./modules/vault"
  environment          = "${var.environment}"
  image_id             = "${var.image_id}"
  instance_type        = "${var.instance_type}"
  key_name             = "${var.key_name}"
  vpc_id               = "${var.vpc_id}"
  public_subnets_id    = "${var.public_subnets_id}"
  public_zone_id       = "${var.public_zone_id}"
}
