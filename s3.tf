resource "aws_s3_bucket" "bucket" {
  bucket = "bjrv-bucket"
}

resource "aws_s3_bucket_policy" "bucket" {
  bucket = "${aws_s3_bucket.bucket.id}"

  policy = <<POLICY
{
	"Version": "2012-10-17",
	"Statement": [{
		"Effect": "Allow",
		"Action": [
			"s3:*",
			"s3:*"
		],
		"Resource": [
			"arn:aws:s3:::bjrv-bucket/*"
		],
		"Principal": "*",
		"Condition": {
			"IpAddress": {
				"aws:sourceIP": ["77.108.144.180/32","10.0.0.0/16"]
			}
		}

	}]
}
POLICY
}
