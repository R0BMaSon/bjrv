variable "access_key" {}
variable "secret_key" {}

variable "region" {
  description = "The region of the infrastucture"
  default = "eu-west-2"
}

variable "environment" {
  description = "The environment"
  default = "bobby-dev"
}

variable "image_id" {
  default = "ami-017b0e29fac27906b"
}

variable "instance_type" {
  default = "t2.small"
}

variable "key_name" {
  description = "The public key for the bastion host"
  default = "robmason-london"
}

variable "vpc_id" {
  default = "vpc-7738c91e"
}

variable "public_subnets_id" {
  type        = "list"
  description = "The CIDR block for the public subnet"
  default = [
    "subnet-8a08f3e3",
    "subnet-02e234b0a17df451a"
  ]
}

variable "public_zone_id" {
  default = "Z2U00Q95U7EKEA"
}
