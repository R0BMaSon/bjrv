# variable "access_key" {}
# variable "secret_key" {}

variable "region" {
  description = "The region of the infrastucture"
  default = "eu-west-2"
}

variable "environment" {
  description = "The environment"
  default = "a4-dev"
}

variable "vpc_cidr" {
  description = "The CIDR block of the vpc"
  default = "10.0.0.0/16"
}

variable "public_subnets_cidr" {
  type        = "list"
  description = "The CIDR block for the public subnet"
  default = [
    "10.0.100.0/24",
    "10.0.101.0/24"
  ]
}

variable "private_subnets_cidr" {
  type        = "list"
  description = "The CIDR block for the private subnet"
  default = [
    "10.0.200.0/24",
    "10.0.201.0/24"
  ]
}

variable "availability_zones" {
  type        = "list"
  description = "The az that the resources will be launched"
  default = [
    "eu-west-2a",
    "eu-west-2b"
  ]
}

variable "key_name" {
  description = "The public key for the bastion host"
  default = "robmason-london"
}

variable "instance_type" {
  default = "t2.micro"
}

variable "image_id" {
  default = "ami-0274e11dced17bb5b"
}

variable "allocated_storage" {
  default = "5"
}

variable "database_username" {
  default = "admin"
}

variable "database_password" {
  default = "password123"
}

variable "database_name" {
  default = "a4"
}

variable "repository_name" {
  default = "bjrv-a4"
}

variable "secret_key_base" {
  default = "thisismysecretkey"
}

variable "execution_role_arn" {
  default = "arn:aws:iam::109964479621:role/ecs_task_execution_role"
}

variable "public_zone_id" {
  default = "Z2U00Q95U7EKEA"
}

variable "database_dns" {
  default = "a4-rds.a4-dev-private.com."
}
