output "vpc_id" {
  value = "${aws_vpc.vpc.id}"
}

output "public_subnets_id" {
  value = ["${aws_subnet.public_subnet.*.id}"]
}

output "private_subnets_id" {
  value = ["${aws_subnet.private_subnet.*.id}"]
}

output "public_sg_id" {
  value = "${aws_security_group.public_sg.id}"
}

output "private_sg_id" {
  value = "${aws_security_group.private_sg.id}"
}
