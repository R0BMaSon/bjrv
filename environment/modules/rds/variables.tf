variable "environment" {
  description = "The environment"
}

variable "private_subnets_id" {
  type        = "list"
  description = "The subnet ids of the private subnets"
}

variable "vpc_id" {
  description = "The vpc id"
}

variable "allocated_storage" {
  description = "Allocated storage of database"
}

variable "database_username" {
  description = "Database username"
}

variable "database_password" {
  description = "Database password"
}

variable "database_name" {
  description = "Database name"
}
