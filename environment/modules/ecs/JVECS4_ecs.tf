resource "aws_ecs_cluster" "cluster" {
  name = "${var.environment}-ecs-cluster"
}

resource "aws_cloudwatch_log_group" "openjobs" {
  name = "openjobs"

  tags {
    Environment = "${var.environment}"
    Application = "OpenJobs"
  }
}

# create ecr repo
resource "aws_ecr_repository" "openjobs_app" {
  name = "${var.repository_name}"
}
