resource "aws_ecs_service" "web" {
  name            = "${var.environment}-web"
  task_definition = "${aws_ecs_task_definition.web.arn}"
  desired_count   = 1
  launch_type     = "FARGATE"
  cluster =       "${aws_ecs_cluster.cluster.id}"
  depends_on      = ["aws_iam_role_policy.ecs_service_role_policy"]

  network_configuration {
    security_groups = ["${aws_security_group.lb.id}"]
    subnets         = ["${var.private_subnets_id}"]
  }

  load_balancer {
    target_group_arn = "${aws_alb_target_group.web.id}"
    container_name   = "web"
    container_port   = 8080
  }

  depends_on = [
    "aws_alb_listener.openjobs",
  ]
}
