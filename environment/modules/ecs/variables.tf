variable "environment" {
  description = "The environment"
}

variable "vpc_id" {
  description = "The vpc id"
}

variable "public_subnets_id" {
  type        = "list"
  description = "The subnet ids of the public subnets"
}

variable "private_subnets_id" {
  type        = "list"
  description = "The subnet ids of the private subnets"
}

variable "repository_name" {
  description = "The name of the ecr repository"
}

variable "secret_key_base" {
  description = "This is a secret key, i dont know what it is exactly"
}

variable "database_username" {
  description = "Database username"
}

variable "database_password" {
  description = "Database password"
}

variable "database_name" {
  description = "Database name"
}

variable "database_endpoint" {
  description = "Database endpoint"
}

variable "execution_role_arn" {
  description = "The tasks execution role"
}

variable "public_zone_id" {
  description = "The public zone id for Route 53"
}
