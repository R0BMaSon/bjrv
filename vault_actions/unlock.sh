#!/bin/bash

Key1=$(grep "Key 1" /Users/vladmoraru/a4git/bjrv/.terraform/modules/fb8a37e65c650f0da5cdce130704772f/vault_keys/keys | cut -d ":" -f2 | xargs)
Key2=$(grep "Key 2" /Users/vladmoraru/a4git/bjrv/.terraform/modules/fb8a37e65c650f0da5cdce130704772f/vault_keys/keys | cut -d ":" -f2 | xargs)
Key3=$(grep "Key 3" /Users/vladmoraru/a4git/bjrv/.terraform/modules/fb8a37e65c650f0da5cdce130704772f/vault_keys/keys | cut -d ":" -f2 | xargs)
Key4=$(grep "Key 4" /Users/vladmoraru/a4git/bjrv/.terraform/modules/fb8a37e65c650f0da5cdce130704772f/vault_keys/keys | cut -d ":" -f2 | xargs)
Key5=$(grep "Key 5" /Users/vladmoraru/a4git/bjrv/.terraform/modules/fb8a37e65c650f0da5cdce130704772f/vault/vault_keys/keys | cut -d ":" -f2 | xargs)
init_token=$(grep "Initial Root Token" /Users/vladmoraru/a4git/bjrv/.terraform/modules/fb8a37e65c650f0da5cdce130704772f/vault_keys/keys | cut -d ":" -f2 | xargs)

for vkey in $Key1 $Key2 $Key3
do
  eval curl -kX PUT -d \'{\"key\": \"$vkey\"}\' https://35.177.98.198:8200/v1/sys/unseal
done
