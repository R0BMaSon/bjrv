#!/bin/bash

init_token=$(grep "Initial Root Token" /Users/vladmoraru/a4git/bjrv/.terraform/modules/fb8a37e65c650f0da5cdce130704772f/vault_keys/keys | cut -d ":" -f2 | xargs)

curl -k -X GET -H "X-Vault-Token:$init_token" https://35.177.98.198:8200/v1/secret/$1 | jq -r '.data.value' 
