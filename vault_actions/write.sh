#!/bin/bash

init_token=$(grep "Initial Root Token" /Users/vladmoraru/a4git/bjrv/.terraform/modules/fb8a37e65c650f0da5cdce130704772f/vault_keys/keys | cut -d ":" -f2 | xargs)

curl -k -X POST -H "X-Vault-Token:$init_token" -d '{"value": "admin"}' https://35.177.98.198:8200/v1/secret/jenkins-username

curl -k -X POST -H "X-Vault-Token:$init_token" -d '{"value": "hello123"}' https://35.177.98.198:8200/v1/secret/jenkins-password

curl -k -X POST -H "X-Vault-Token:$init_token" -d '{"value": "admin"}' https://35.177.98.198:8200/v1/secret/database-username

curl -k -X POST -H "X-Vault-Token:$init_token" -d '{"value": "password123"}' https://35.177.98.198:8200/v1/secret/database-password
