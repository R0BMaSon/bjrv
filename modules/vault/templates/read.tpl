#!/bin/bash

init_token=$(grep "Initial Root Token" ${path}/vault_keys/keys | cut -d ":" -f2 | xargs)

curl -k -X GET -H "X-Vault-Token:$init_token" https://${vault_ip}:8200/v1/secret/$1 | jq -r '.data.value' 
