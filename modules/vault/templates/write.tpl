#!/bin/bash

init_token=$(grep "Initial Root Token" ${path}/vault_keys/keys | cut -d ":" -f2 | xargs)

curl -k -X POST -H "X-Vault-Token:$init_token" -d '{"value": "admin"}' https://${vault_ip}:8200/v1/secret/jenkins-username

curl -k -X POST -H "X-Vault-Token:$init_token" -d '{"value": "hello123"}' https://${vault_ip}:8200/v1/secret/jenkins-password

curl -k -X POST -H "X-Vault-Token:$init_token" -d '{"value": "admin"}' https://${vault_ip}:8200/v1/secret/database-username

curl -k -X POST -H "X-Vault-Token:$init_token" -d '{"value": "password123"}' https://${vault_ip}:8200/v1/secret/database-password
