#!/bin/bash

Key1=$(grep "Key 1" ${path}/vault_keys/keys | cut -d ":" -f2 | xargs)
Key2=$(grep "Key 2" ${path}/vault_keys/keys | cut -d ":" -f2 | xargs)
Key3=$(grep "Key 3" ${path}/vault_keys/keys | cut -d ":" -f2 | xargs)
Key4=$(grep "Key 4" ${path}/vault_keys/keys | cut -d ":" -f2 | xargs)
Key5=$(grep "Key 5" ${path}/vault/vault_keys/keys | cut -d ":" -f2 | xargs)
init_token=$(grep "Initial Root Token" ${path}/vault_keys/keys | cut -d ":" -f2 | xargs)

for vkey in $Key1 $Key2 $Key3
do
  eval curl -kX PUT -d \'{\"key\": \"$vkey\"}\' https://${vault_ip}:8200/v1/sys/unseal
done
