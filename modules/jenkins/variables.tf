variable "environment" {
  description = "The environment"
}

variable "image_id" {
  description = "The image id of the instances"
}

variable "instance_type" {
  description = "The instance type"
}

variable "key_name" {
  description = "The key used for the instances"
}

variable "vpc_id" {
  description = "The vpc id"
}

variable "public_subnets_id" {
  type        = "list"
  description = "The subnet ids of the public subnets"
}

variable "public_zone_id" {
  description = "The public zone id for Route 53"
}
