resource "aws_security_group" "jenkins_sg" {
    name = "${var.environment}-jenkins-sg"
    description = "jenkins security group"

    ingress {
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = ["77.108.144.180/32"]
    }
    ingress {
        from_port = 8080
        to_port = 8080
        protocol = "tcp"
        cidr_blocks = ["77.108.144.180/32"]
    }
    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["77.108.144.180/32"]
    }
    egress {
        from_port   = 0
        to_port     = 0
        protocol    = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
    vpc_id = "${var.vpc_id}"

    tags {
      Name        = "${var.environment}-public-sg"
      Environment = "${var.environment}"
    }
}

resource "aws_instance" "jenkins" {
    ami = "${var.image_id}"
    vpc_security_group_ids = [ "${aws_security_group.jenkins_sg.id}" ]
    instance_type = "${var.instance_type}"
    key_name      = "${var.key_name}"
    associate_public_ip_address = true
    iam_instance_profile = "${aws_iam_instance_profile.jenkins_profile.name}"
    tags {
        Name = "${var.environment}-jenkins"
    }
    subnet_id = "${element(var.public_subnets_id, count.index)}"

    depends_on = ["aws_instance.jenkins"]

    provisioner "local-exec" {
      command = "rm ${path.module}/ansible/hosts && sleep 30"
    }

    provisioner "local-exec" {
      command = "echo \"[jenkins]\" > ${path.module}/ansible/hosts"
    }

    provisioner "local-exec" {
      command = "echo ${aws_instance.jenkins.public_ip} >> ${path.module}/ansible/hosts"
    }

    provisioner "local-exec" {
      command     = "ansible-playbook -i ${path.module}/ansible/hosts ${path.module}/ansible/site.yml"
    }
}

resource "aws_route53_record" "jenkins" {
      zone_id = "${var.public_zone_id}"
      name = "bobby-jenkins"
      type = "CNAME"
      ttl = "30"
      records = ["${aws_instance.jenkins.public_dns}"]
}

resource "aws_iam_role" "jenkinsrole" {
  name = "${var.environment}-jenkinsrole"
  path = "/"

  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "sts:AssumeRole",
            "Principal": {
               "Service": "ec2.amazonaws.com"
            },
            "Effect": "Allow",
            "Sid": ""
        }
    ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "jenkinsattachasdmin" {
  role       = "${aws_iam_role.jenkinsrole.name}"
  policy_arn = "arn:aws:iam::aws:policy/AdministratorAccess"
}

resource "aws_iam_role_policy_attachment" "jenkinsattach" {
  role       = "${aws_iam_role.jenkinsrole.name}"
  policy_arn = "arn:aws:iam::109964479621:policy/JenkinsPolicy"
}

resource "aws_iam_instance_profile" "jenkins_profile" {
  name = "${var.environment}-jenkins_profile"
  role = "${aws_iam_role.jenkinsrole.name}"
}
